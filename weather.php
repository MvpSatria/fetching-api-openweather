<?php

$city = 'Singaraja';
$apiKey = 'de92798925116cecb8ab3e83c181891d';
$url =  'https://api.openweathermap.org/data/2.5/weather?q=' . $city . '&appid=' . $apiKey . '&units=metric';

$request = file_get_contents($url);
$response = json_decode($request, true);

$kota = $response['name'];
$cuaca = $response['weather'][0]['main'];
$temperatur = $response['main']['temp'];
$kelembaban = $response['main']['humidity'];

if ($cuaca == 'Clouds') {
    $ikon = '<i class="fa-solid fa-cloud fa-xs"></i>';
} elseif ($cuaca == 'Clear') {
    $ikon = '<i class="fa-solid fa-sun"></i>';
} elseif ($cuaca == 'Rain') {
    $ikon = '<i class="fa-solid fa-cloud-rain"></i>';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Weather</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <div class="container">
        <div class="text-white text-center">
            <h1 class="mb-3 mt-5">Weather Condition</h1>
            <form class="d-flex col-6 ml-auto mr-auto">
                <input class="form-control mr-1" type="search" placeholder="Search..." aria-label="Search">
                <button class="btn btn-success" type="submit">Search</button>
            </form>
        </div>
        <div class="d-flex justify-content-start">
            <div class="card-transparent text-white">
                <div class=" card-body">
                    <h1 class="card-title"><?php echo $kota . ' ' . $ikon ?></h1>
                    <p class="card-text"><?php echo $cuaca . ' | ' . $temperatur ?>&#8451</p>
                    <p class="card-text"><?php echo $kelembaban ?>% Kelembaban</p>
                    <h5 class="card-text mb-3"><?php echo date("Y/m/d") ?></h5>
                    <a href="#" class="btn btn-outline-primary">MORE</a>
                </div>
            </div>
        </div>
    </div>
</body>

</html>